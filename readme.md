# Find-a-scan Refreshment

## Objectives

* Replace an existing and somewhat dated applications
* Streamline process of searching and retrieving file scans
* Enhance search functionality
* Support high-availability for the file scans and search functionality

## Business Process Overview

* An end-user scans a document using a physical scanner
* Scanner uploads the scan to a Windows Server
* Files are stored on the server and are organized in folders and sub-folders
* An existing batch process is in place to copy files to another Windows Server
* End-user leverages a modern browser UI to locate file(s) for printing or for downloading purposes

## Technical Constraints & Assumptions

* Windows Server 2012 is in use to store scanned files
* It is possible to configure an FTP/sftp server and the firewall on the scanner server to facilitate remote file transfer
* It is possible to install a new VM with a desired Linux distribution to retrieve the scanned documents or to expose the folder structure using sftp server
* Scanned files can be stored on public cloud given proper security measures
* Path to a scanned document is the primarily search criteria, at least, for the MVP

## Proposed Solutions

### MVP: On-Premises

![](./media/on-prem.jpeg)

* Setup a small VM with a Linux distribution to server as a file collector
* Install WinSCP and configure it to run in a [batch mode](https://winscp.net/eng/docs/scripting)
* Inside VM run a Node.JS service/daemon to monitor files uploaded by WinSCP process
* Install 3-nodes Cluster for ElasticSearch, Kibana, and X-Pack on Linux VM's: 2CPU & 4GB of RAM
* Node.JS process indexed path and filename into Elastic Search Cluster for new/updated files
* End-users access Kibana on through a load-balancer to search for scans using free text searches
* Backup process will be required for ElasticSearch to create periodic snapshots for disaster recovery purposes

### MVP: Hybrid

![](./media/hybrid.jpeg)

* Setup a small VM with a Linux distribution to server as a folder watcher and a file collector
* Inside VM run a Node.JS service/daemon to monitor files the scanner server or on an FTP server
* For the initial data migration the same Node.JS process should pickup and upload all existing files
* Node.JS process will upload new/updated files to a highly available AWS S3 storage for processing and backup purposes

### MVP: Cloud

![](./media/cloud.jpeg)

* Setup sFtp server on-premises to securely expose folder structure with scanned documents for remote access
* Node.JS scheduled task running on Aws will connect over sFtp protocol to process and will upload new/updated files to a highly available AWS S3 storage for processing and backup purposes

### MVP Common

* S3 file upload event triggers a Node.JS Lambda function to process the new/updated file
* Lambda function extracts the full path name to turn it into searchable tokens
* Lambda function indexes the file path and any other meta-data elements from the source file with a link to a S3 object into a hosted Elastic Search Cluster
* Kibana web based user interface provides search for the scanned documents by tags such as file/path name
* User interface will presents link to download the original document to end-user desktop/laptop from S3 bucket
* End-user authentication uses AWS Cognito for the MVP and subsequently can be converted to an on-premises Microsoft Active Directory authentication
* Sample Kibana UI with search criteria, items found using the search criteria highlighted, and a link to download file from S3 bucket (may pop-up a login screen again)
![](./media/kibana-dashboard.png)

### Min Hosting Cost Estimate USD

| Item | Monthly Cost | Comments |
| --- | --- | --- |
| 1TB S3 Infrequent Access Storage | $14.13 | https://aws.amazon.com/s3/pricing/ |
| 10GB Traffic | $1.50 | https://aws.amazon.com/blogs/aws/aws-data-transfer-prices-reduced/ |
| t2.small.elasticsearch | $27.36 | https://aws.amazon.com/elasticsearch-service/pricing/ |
| Lambda <=1M | $0.00 | https://aws.amazon.com/lambda/pricing/ |
| Cognito <=50 users | $0.00 | https://aws.amazon.com/cognito/pricing |
| Total | $42.99 | |

### Max Hosting Cost Estimate USD

| Item | Monthly Cost | Comments |
| --- | --- | --- |
| 1TB S3 Infrequent Access Storage | $14.13 | https://aws.amazon.com/s3/pricing/ |
| 10GB Traffic | $1.50 | https://aws.amazon.com/blogs/aws/aws-data-transfer-prices-reduced/ |
| t2.medium.elasticsearch | $55.44 | https://aws.amazon.com/elasticsearch-service/pricing/ |
| Lambda <=1M | $0.00 | https://aws.amazon.com/lambda/pricing/ |
| Cognito <=50 users | $0.00 | https://aws.amazon.com/cognito/pricing |
| Total | $71.07 | |

### Post MVP Enhancements

* Integrate Aws with on-premises Microsoft Active directory for [SSO purposes](https://docs.aws.amazon.com/singlesignon/latest/userguide/connectawsad.html)
* Leverage Aws [Rekognition](https://docs.aws.amazon.com/rekognition/latest/dg/text-detection.html) to perform an OCR on the scanned document to enrich search capabilities
* Enable [S3 versions](https://docs.aws.amazon.com/AmazonS3/latest/dev/Versioning.html) for scans to facilitate storage and retrieval of previous versions
* Enable bucket [access auditing](https://docs.aws.amazon.com/AmazonS3/latest/user-guide/server-access-logging.html) for possible security/compliance purposes

### MVP Scope, Timeline and Budget

* Node.JS service running inside a docker container based on one of the [official images](https://store.docker.com/images/node)
  * Or Node.JS process is monitoring/periodically scanning a remote (s)FTP folder to pickup and to upload scanned documents to S3 bucket
  * Or aws-cli to sync entire source folder to Aws S3: https://docs.aws.amazon.com/cli/latest/reference/s3/sync.html
* Monitoring of on-premises (s)Ftp server, container host and container running locally is **out-of-scope**
* Node.JS process persists execution log on a local IO or to CloudWatch Logs, as well as uploads the logs to ElasticSearch for troubleshooting/monitoring purposes
* Provisioning of required AWS resources: S3, Aws Lambda, ElasticSearch, and Cognito
* Kibana search and dashboard setup
* A walk through on managing users using Cognito
* A walk through on monitoring and troubleshooting Aws assets
* A walk through on monitoring and troubleshooting Node.JS pickup service
* Estimated delivery: first iteration within a week, end-to-end demo in a sandbox environment ~2weeks, demo of on-premises setup in ~3weeks

### Financial Terms for On-Premises Architecture

* Fixed quote budget: $7,000CAD + hst. Warranty: 60 days from the on-premises end-to-end demo. Backup, logs aggregations, and monitoring is not included. Source code and the data is owned by the client

### Financial Terms for Cloud or Hybrid Architecture

* Fixed quote budget: $5,000CAD + hst. Warranty: 60 days from the on-premises end-to-end demo. Hosting fees are not included. Deployment is on the client AWS account. Source code and the data is owned by the client
* SaaS model: $566.67 CAD paid monthly + hst including hosting fees. The first 12 months client is committed to the contract with an option for an early termination with a refund of $150 per remaining month. E.g. the value of the contract for the first 12 months is $6,800.04 + hst, should the client terminate the contract after 6 months cost of termination will be ($566.67 * 6 - $150 * 6) = $2,500.02 + hst. Past the first 12 months month-to-month ongoing maintenance and enhancements are included. Each party reserves the right to terminate the contract for no reason on a 60-days notice. The service provider is to provide data backup/snapshot in case of contract termination. Open source-code mode where the data is owned by the client while safe-guarded by the service provider.
